import discord
import os
from replit import db
from keep_alive import keep_alive

client = discord.Client()

@client.event
async def on_message(ctx):
  print('message:'+str(ctx))

  ctx_channel = ctx.channel
  ctx_author_spe_char = ctx.author
  ctx_content = ctx.content
  bot_message = ""
  _bids = []
  
  if ctx_channel.name == 'dkp-beta':
    try: 
      if ctx_author_spe_char == client.user:
        return
      
      await ctx_channel.send(f'Processing the data, please wait.')
      
      # print(f'ctx_author_spe_char: {ctx_author_spe_char.name}')
      ctx_author = ''.join(char for char in ctx_author_spe_char.name if char.isalnum())

      if ctx_content.lower().startswith('bid'):

        print('ctx_content:'+str(ctx_content))
        new_bid_value = int(ctx_content[3:].strip())
        print('message.bid new_bid_value value **:'+str(new_bid_value))
        if (new_bid_value > 0 and
          new_bid_value < 300000000 and
          isinstance(new_bid_value, int)):
          _bids.clear()
          for key in db.keys():
            _bids.append(db[key])
          if new_bid_value in _bids:
            bot_message = f"Error occured.\nBid value already entered. Choose another one.\nUse only positive numbers after ***Bid***.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***";
            await ctx_channel.send(bot_message)
          else:
            db[ctx_author] = new_bid_value

            _bids.clear()
            for key in db.keys():
              _bids.append(db[key])
            bot_message='```'
            _bids.sort(reverse = True)
            max_bids = 0
            if len(_bids) > 20:
              max_bids = 20
            else:
              max_bids = len(_bids)
            bot_message = bot_message + f'\n| Rank | Name            |  DKP bid  |'
            for i in range(max_bids):
              for key in db.keys():
                if db[key] == _bids[i]:
                  bot_message = bot_message + f'\n|  {i+1:02d}  | {key:15s} |  {_bids[i]:,}  |'
            bot_message = bot_message + '```\nCurrent top 15 ranks for upcoming MG are above, 5 more displayed incase we need to dequeue someone.\nEnter a bid value from your DKP stash found in https://datastudio.google.com/s/oilNBQ2nqPM.\nYou are removed from the queue if you bid more points than you have after the rankings are locked.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***. You may opt out with ***Bid 0*** command.\n If bot says ***Processing the data, please wait*** , do not enter any command until it prints the result.'
            await ctx_channel.purge(limit=100)
            await ctx_channel.send(bot_message)
        elif new_bid_value == 0:
          print(f'Player {ctx_author} decided to opt out of bidding')
          if ctx_author in db.keys():
            del db[ctx_author]
            
            _bids.clear()
            for key in db.keys():
              _bids.append(db[key])
            bot_message='```'
            _bids.sort(reverse = True)
            max_bids = 0
            if len(_bids) > 20:
              max_bids = 20
            else:
              max_bids = len(_bids)
            bot_message = bot_message + f'\n| Rank | Name            |  DKP bid  |'
            for i in range(max_bids):
              for key in db.keys():
                if db[key] == _bids[i]:
                  bot_message = bot_message + f'\n|  {i+1:02d}  | {key:15s} |  {_bids[i]:,}  |'
            bot_message = bot_message + '```\nCurrent top 15 ranks for upcoming MG are above, 5 more displayed incase we need to dequeue someone.\nEnter a bid value from your DKP stash found in https://datastudio.google.com/s/oilNBQ2nqPM.\nYou are removed from the queue if you bid more points than you have after the rankings are locked.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***. You may opt out with ***Bid 0*** command.\n If bot says ***Processing the data, please wait*** , do not enter any command until it prints the result.'
            await ctx_channel.purge(limit=100)
            await ctx_channel.send(bot_message)
          else:
            print(f'Player {ctx_author} already opted out of bidding')
            bot_message = f'Error occured.\nPlayer {ctx_author} already opted out or never applied a bid.\nIf you want to bid, enter a new bid in the format: ***Bid XXX***. Example: ***Bid 135670***'
            await ctx_channel.send(bot_message)
        else:
          bot_message = f'Error occured.\nBid value entered by {ctx_author} is {new_bid_value}. Bid cannot be a negative number or greater than 300m.\nUse only positive numbers after ***Bid***.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***'
          await ctx_channel.send(bot_message)
      elif ctx_content.lower().startswith('refresh') and ctx_author_spe_char.id == 165253442292547584:
        _bids.clear()
        for key in db.keys():
          _bids.append(db[key])
        bot_message='```'
        _bids.sort(reverse = True)
        max_bids = 0
        if len(_bids) > 20:
          max_bids = 20
        else:
          max_bids = len(_bids)
        bot_message = bot_message + f'\n| Rank | Name            |  DKP bid  |'
        for i in range(max_bids):
          for key in db.keys():
            if db[key] == _bids[i]:
              bot_message = bot_message + f'\n|  {i+1:02d}  | {key:15s} |  {_bids[i]:,}  |'
        bot_message = bot_message + '```\nCurrent top 15 ranks for upcoming MG are above, 5 more displayed incase we need to dequeue someone.\nEnter a bid value from your DKP stash found in https://datastudio.google.com/s/oilNBQ2nqPM.\nYou are removed from the queue if you bid more points than you have after the rankings are locked.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***. You may opt out with ***Bid 0*** command.\n If bot says ***Processing the data, please wait*** , do not enter any command until it prints the result.'
        await ctx_channel.purge(limit=100)
        await ctx_channel.send(bot_message)
      elif ctx_content.lower().startswith('cleardb') and ctx_author_spe_char.id == 165253442292547584:
        # print('clearing DB')
        db.clear()
        await ctx_channel.purge(limit=100)
        # print(f'printing db.keys() after ClearDB command:{str(db.keys())}')
        bot_message = 'MGE ended, wait for the next one to start.\nCleared the previous MG database'
        await ctx_channel.send(bot_message)
      elif ctx_content.lower().startswith('cleanmsgs') and ctx_author_spe_char.id == 165253442292547584:
        # print('clearing messages')
        await ctx_channel.purge(limit=100)
        bot_message = 'MGE ended, wait for the next one to start.\nCleared the channel messages'
        await ctx_channel.send(bot_message)
      elif ctx_content.lower().startswith('printdb') and ctx_author_spe_char.id == 165253442292547584:
        # print('PrintDB: ' + str(db.keys()))
        bot_message = f'MGE ended, wait for the next one to start.\nPrinting all MG entries from DB: {str(db.keys())}'
        await ctx_channel.send(bot_message)
      else:
        bot_message = f'Command does not exist.\nBid value not entered properly by {ctx_author}.\nUse only positive numbers after ***Bid***.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***'
        await ctx_channel.send(bot_message)

    except:
        bot_message = f'Error occured.\nBid value not entered properly by {ctx_author}.\nEnter in the format: ***Bid XXX***. Example: ***Bid 135670***'
        await ctx_channel.send(bot_message)

keep_alive()
client.run(os.getenv('TOKEN'))
